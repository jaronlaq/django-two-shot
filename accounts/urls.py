from django.urls import path
from accounts.views import login_form, logout_acc, user_signup

urlpatterns = [
    path('login/', login_form, name='login'),
    path('logout/', logout_acc, name='logout'),
    path("signup/", user_signup, name='signup'),
]